//
//  LocalDataHandler.h
//
//  Created by Smiljan Kerencic on 30. 06. 14.
//  Copyright (c) 2014. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocalDataHandler : NSObject

+ (void) clearAll;
//+ (void) clearAllButDeviceData;

+ (NSString*) readStringForKey:(NSString*)key withDefault:(NSString*)defValue;
+ (int) readIntForKey:(NSString*)key withDefault:(int)defValue;
+ (float) readFloatForKey:(NSString*)key withDefault:(float)defValue;
+ (BOOL) readBoolForKey:(NSString*)key withDefault:(BOOL)defValue;
+ (long) readLongForKey:(NSString*)key withDefault:(long)defValue;
+ (NSDate*) readDateForKey:(NSString*)key withDefault:(NSDate*)defValue;
+ (id) readObjectForKey:(NSString*)key;

+ (BOOL) writeString:(NSString*)obj forKey:(NSString*)key;
+ (BOOL) writeInt:(int)obj forKey:(NSString*)key;
+ (BOOL) writeFloat:(float)obj forKey:(NSString*)key;
+ (BOOL) writeBool:(BOOL)obj forKey:(NSString*)key;
+ (BOOL) writeLong:(long)obj forKey:(NSString*)key;
+ (BOOL) writeDate:(NSDate*)obj forKey:(NSString*)key;
+ (BOOL) writeObject:(id)obj forKey:(NSString*)key;

@end
