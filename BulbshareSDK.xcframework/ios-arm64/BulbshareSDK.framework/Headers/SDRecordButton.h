//
//  SDRecordButton.h
//  SDRecordButton
//
//  Created by Sebastian Dobrincu on 13/08/15.
//  Copyright (c) 2015 Sebastian Dobrincu. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SDRecordButton;

@protocol SDRecordButtonDelegate <NSObject>
- (void)recorderTakePhoto;
- (void)recorderTakeVideo;
- (void)recorderDidEndTakingVideo;
- (void)recorderDidEndTakingPhoto;
@end

@interface SDRecordButton : UIButton

@property (nonatomic, strong) IBInspectable UIColor *buttonColor;
@property (nonatomic, strong) IBInspectable UIColor *progressColor;
@property (nonatomic, strong) CALayer *circleLayer;
@property (nonatomic, strong) CALayer *circleBorder;
@property (nonatomic, strong) CAShapeLayer *progressLayer;
@property (nonatomic, strong) CAGradientLayer *gradientMaskLayer;

@property (weak) id<SDRecordButtonDelegate> delegate;

- (void)setProgress:(CGFloat)newProgress;
- (CGFloat)getProgress;

@end

