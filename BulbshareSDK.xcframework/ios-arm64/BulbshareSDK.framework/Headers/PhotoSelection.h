//
//  PhotoSelection.h
//  APMediaLibraryViewController
//
//  Created by CreoleStuduios on 6/15/17.
//  Copyright © 2017 CreoleStudios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APGradientView.h"
@interface PhotoSelection : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgCheck;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UIView *viewGreen;
@property (weak, nonatomic) IBOutlet UIView *viewIsVideo;
@property (weak, nonatomic) IBOutlet UILabel *labelVideoDuration;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet APGradientView *videoGradientView;

@end

