#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface APBackgroundManager : NSObject

@property(nonatomic, readonly) BOOL isTaskStarted;

-(void)startTask;
-(void)endTask;
-(BOOL)isInBackground;

@end
