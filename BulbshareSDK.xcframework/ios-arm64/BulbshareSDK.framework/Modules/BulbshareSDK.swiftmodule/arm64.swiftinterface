// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.5.1 (swiftlang-1300.0.31.4 clang-1300.0.29.6)
// swift-module-flags: -target arm64-apple-ios12.1 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name BulbshareSDK
import AVFoundation
import AVKit
import Alamofire
@_exported import BulbshareSDK
import CommonCrypto
import CoreAudio
import CoreData
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit
import Foundation
import GoogleSignIn
import ImageIO
import KeychainAccess
import MBProgressHUD
import Swift
import SwiftUI
import SystemConfiguration
import TwitterKit
import UIKit
import WebKit
import _Concurrency
public protocol ImagePickerDelegate : AnyObject {
  func didSelect(image: UIKit.UIImage?)
}
@objc open class ImagePicker : ObjectiveC.NSObject {
  public init(presentationController: UIKit.UIViewController, delegate: BulbshareSDK.ImagePickerDelegate)
  public func pickWithoutEditing()
  public func presentImagePicker(from sourceView: UIKit.UIView, sourcetype: UIKit.UIImagePickerController.SourceType)
  @objc deinit
}
extension BulbshareSDK.ImagePicker : UIKit.UIImagePickerControllerDelegate {
  @_Concurrency.MainActor(unsafe) @objc dynamic public func imagePickerControllerDidCancel(_ picker: UIKit.UIImagePickerController)
  @_Concurrency.MainActor(unsafe) @objc dynamic public func imagePickerController(_ picker: UIKit.UIImagePickerController, didFinishPickingMediaWithInfo info: [UIKit.UIImagePickerController.InfoKey : Any])
}
extension BulbshareSDK.ImagePicker : UIKit.UINavigationControllerDelegate {
}
@_inheritsConvenienceInitializers @objc public class BSSDKConfig : ObjectiveC.NSObject {
  public init(appVersion: Swift.String, appName: Swift.String, appId: Swift.String, apiKey: Swift.String, wlaKey: Swift.String)
  @objc override dynamic public init()
  @objc deinit
}
@_inheritsConvenienceInitializers @objc public class SDKBundles : ObjectiveC.NSObject {
  @objc public class func bundle() -> Foundation.Bundle?
  @objc override dynamic public init()
  @objc deinit
}
public struct BSUserRefInput : Swift.Encodable {
  public init(userRef: Swift.String)
  public func encode(to encoder: Swift.Encoder) throws
}
public struct BSBulbshareListInput : Swift.Encodable {
  public init(briefRef: Swift.String, count: Swift.Int, sortType: Swift.Int)
  public func encode(to encoder: Swift.Encoder) throws
}
public struct BSBriefInput : Swift.Encodable {
  public init(privateBrandId: Swift.Int)
  public func encode(to encoder: Swift.Encoder) throws
}
@_inheritsConvenienceInitializers @objc public class Localization : ObjectiveC.NSObject {
  @objc public static let submitCloseTitleText: Swift.String
  @objc public static let createBulbshare: Swift.String
  @objc public static let enterTitleHere: Swift.String
  @objc public static let enterTextHere: Swift.String
  @objc public static let closeSubmitText: Swift.String
  @objc public static let storeSubmitText: Swift.String
  @objc public static let submitCloseMessageText: Swift.String
  @objc public static let downloadingVideo: Swift.String
  @objc public static let exportingVideo: Swift.String
  @objc public static let addTitle: Swift.String
  @objc public static let unableToImportVideo: Swift.String
  @objc public static let unableToImportImage: Swift.String
  @objc public static let createYourBulbshareLongVideo: Swift.String
  @objc public static let tapToTakePhoto: Swift.String
  @objc public static let videoLengthLimitTitle: Swift.String
  @objc public static let videoLengthLimitMessage: Swift.String
  @objc override dynamic public init()
  @objc deinit
}
@_inheritsConvenienceInitializers @objc public class BaseTheme : ObjectiveC.NSObject {
  @objc public static var theme: BulbshareSDK.BaseTheme
  @objc public var channelId: Swift.Int
  @objc public func setSubmitBulbshareButtonColor(channelId: Swift.Int) -> UIKit.UIColor
  @objc public func setSubmitBulbshareButtonTextColor(channelId: Swift.Int) -> UIKit.UIColor
  @objc public func setLastIconImageTrayColor(channelId: Swift.Int) -> UIKit.UIColor
  @objc public func setMediaLibraryTopPartColor(channelId: Swift.Int) -> UIKit.UIColor
  @objc public func setCloseIconOnImageVideoColor(channelId: Swift.Int) -> UIKit.UIColor
  @objc public func setSubmitProgressColor(channelId: Swift.Int) -> UIKit.UIColor
  @objc public func setCommentColor()
  @objc override dynamic public init()
  @objc deinit
}
@_hasMissingDesignatedInitializers @objc public class Device : ObjectiveC.NSObject {
  @objc public var device_os: Swift.String!
  @objc deinit
}
@_inheritsConvenienceInitializers @objc(CDPollBrief) public class CDPollBrief : CoreData.NSManagedObject {
  @objc override dynamic public init(entity: CoreData.NSEntityDescription, insertInto context: CoreData.NSManagedObjectContext?)
  @objc deinit
}
public struct BSBriefRefInput : Swift.Encodable {
  public init(briefRef: Swift.String)
  public func encode(to encoder: Swift.Encoder) throws
}
public struct BSSdkThemeInput {
  public var loadingProgressColor: BulbshareSDK.ThemeColor?
  public var sendButtonColor: BulbshareSDK.ThemeColor?
  public var tabIndicatorColor: BulbshareSDK.ThemeColor?
  public var tabIndicatorTextColor: BulbshareSDK.ThemeColor?
  public var lazyLoadingSpinnerColor: BulbshareSDK.ThemeColor?
  public var activeTimeLeftColor: BulbshareSDK.ThemeColor?
  public var completedTimeLeftColor: BulbshareSDK.ThemeColor?
  public var shareButtonBgColor: BulbshareSDK.ThemeColor?
  public var myProfileBannerColor: BulbshareSDK.ThemeColor?
  public var socialMediaNameColor: BulbshareSDK.ThemeColor?
  public var imageHighlightBGColor: BulbshareSDK.ThemeColor?
  public var pollImageBgLogo: UIKit.UIImage?
  public init(primaryColor: BulbshareSDK.ThemeColor, secondaryColor: BulbshareSDK.ThemeColor)
}
public struct ThemeColor {
  public var r: Swift.Int, g: Swift.Int, b: Swift.Int, a: Swift.Int
  public init(r: Swift.Int, g: Swift.Int, b: Swift.Int, a: Swift.Int)
}
@_inheritsConvenienceInitializers @objc public class KeychainAccessHandler : ObjectiveC.NSObject {
  @objc public func getUserSecret() -> Swift.String?
  @objc public func getUserRef() -> Swift.String?
  @objc public func getDeviceRef() -> Swift.String?
  @objc override dynamic public init()
  @objc deinit
}
extension BulbshareSDK.CDPollBrief {
  @nonobjc public class func fetchRequest() -> CoreData.NSFetchRequest<BulbshareSDK.CDPollBrief>
  @objc @NSManaged dynamic public var isUploaded: Swift.Bool {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var ref: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var response: Foundation.NSSet? {
    @objc get
    @objc set
  }
}
extension BulbshareSDK.CDPollBrief {
  @objc(addResponseObject:) @NSManaged dynamic public func addToResponse(_ value: BulbshareSDK.CDPollResponse)
  @objc(removeResponseObject:) @NSManaged dynamic public func removeFromResponse(_ value: BulbshareSDK.CDPollResponse)
  @objc(addResponse:) @NSManaged dynamic public func addToResponse(_ values: Foundation.NSSet)
  @objc(removeResponse:) @NSManaged dynamic public func removeFromResponse(_ values: Foundation.NSSet)
}
public protocol BSAuthenticatioinDelegate : BulbshareSDK.BSErrorDelegate {
  func onSuccessAuthentication()
}
public protocol BSBriefDelegate : BulbshareSDK.BSErrorDelegate {
  func onSuccessBriefLoaded()
}
public protocol BSErrorDelegate : AnyObject {
  func onSDKError(error: Swift.Error)
}
public protocol BSBulbshareDelegate : BulbshareSDK.BSErrorDelegate {
  func onSuccessBriefLoaded()
}
public protocol BSBulbshareFeedDelegate : BulbshareSDK.BSErrorDelegate {
  func onSuccessBriefLoaded()
}
public protocol BSRewardsDelegate : BulbshareSDK.BSErrorDelegate {
  func onSuccessRewardsLoaded()
}
public protocol BSPollBriefDdelegate : BulbshareSDK.BSErrorDelegate {
  func onSuccessPollBriefLoaded()
}
public protocol BSBriefIntroDelegate : BulbshareSDK.BSErrorDelegate {
  func onSuccessBriefIntroLoaded()
}
public protocol BSSingleBulbshareDelegate : BulbshareSDK.BSErrorDelegate {
  func onSuccessSingleBulbshareLoaded()
}
public protocol BSCommentDelegate : BulbshareSDK.BSErrorDelegate {
  func onSuccessCommentLoaded()
}
public protocol BSFeedDelegate : BulbshareSDK.BSErrorDelegate {
  func onSuccessFeedLoaded()
}
public protocol BSCreateSubmitDelegate : BulbshareSDK.BSErrorDelegate {
  func onSuccessCreateSubmitDelegate()
}
public protocol BSMyProfileDelegate : BulbshareSDK.BSErrorDelegate {
  func onSuccessMyProfileLoaded()
}
extension BulbshareSDK.CDPollCollection {
  @nonobjc public class func fetchRequest() -> CoreData.NSFetchRequest<BulbshareSDK.CDPollCollection>
  @objc @NSManaged dynamic public var comment: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var mediaType: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var optionid: Swift.Int32 {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var relX: Swift.Double {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var relY: Swift.Double {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var sentiment: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var value: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var video_tn: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var response: BulbshareSDK.CDPollResponse? {
    @objc get
    @objc set
  }
}
public struct BSUnlinkInput : Swift.Encodable {
  public init(useraccountid: Swift.Int)
  public func encode(to encoder: Swift.Encoder) throws
}
@_hasMissingDesignatedInitializers public class BulbshareClient {
  public static func shared(config: BulbshareSDK.BSSDKConfig) -> BulbshareSDK.BulbshareClient
  weak public var authenticationDelegate: BulbshareSDK.BSAuthenticatioinDelegate?
  public func authenticate(input: BulbshareSDK.BSAuthenticationInput)
  public func setTheme(channelId: Swift.Int, theme: BulbshareSDK.BSSdkThemeInput)
  weak public var feedDelegate: BulbshareSDK.BSFeedDelegate?
  public func showFeed(viewController: UIKit.UIViewController, input: BulbshareSDK.BSBriefInput)
  weak public var pollBriefDelegate: BulbshareSDK.BSPollBriefDdelegate?
  public func showPollBrief(viewController: UIKit.UIViewController, input: Swift.String)
  weak public var briefDelegate: BulbshareSDK.BSBriefDelegate?
  public func showBrief(viewController: UIKit.UIViewController, input: BulbshareSDK.BSBriefInput)
  weak public var briefIntroDelegate: BulbshareSDK.BSBriefIntroDelegate?
  public func showBriefIntro(viewController: UIKit.UIViewController, input: BulbshareSDK.BSBriefRefInput)
  weak public var singleBulbshareDelegate: BulbshareSDK.BSSingleBulbshareDelegate?
  public func showSingleBulbshare(viewController: UIKit.UIViewController, input: BulbshareSDK.BSSingleBulbshareInput)
  weak public var bulbshareListDelegate: BulbshareSDK.BSBulbshareDelegate?
  public func showBulbshareList(viewController: UIKit.UIViewController, input: BulbshareSDK.BSBulbshareListInput)
  weak public var bulbshareFeedDelegate: BulbshareSDK.BSBulbshareFeedDelegate?
  public func showBulbshareFeed(viewController: UIKit.UIViewController, input: BulbshareSDK.BSBulbshareFeedInput)
  weak public var rewardsDelegate: BulbshareSDK.BSRewardsDelegate?
  public func showRewards(viewController: UIKit.UIViewController)
  weak public var commentDelegate: BulbshareSDK.BSCommentDelegate?
  public func showComment(viewController: UIKit.UIViewController, input: BulbshareSDK.BSBriefRefInput)
  weak public var createBulshareDelegate: BulbshareSDK.BSCreateSubmitDelegate?
  public func showcreateBulbshare(breifRef: Swift.String, viewController: UIKit.UIViewController, type: Swift.Int)
  public func handleUnSubmittedSurvey()
  weak public var myProfileDelegate: BulbshareSDK.BSMyProfileDelegate?
  public func showMyProfile(viewController: UIKit.UIViewController, input: BulbshareSDK.BSUserRefInput)
  @objc deinit
}
public struct LinkInputModel : Swift.Codable {
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
@_inheritsConvenienceInitializers @objc public class DeviceManager : ObjectiveC.NSObject {
  @objc public static let shared: BulbshareSDK.DeviceManager
  @objc public func device() -> BulbshareSDK.Device
  @objc override dynamic public init()
  @objc deinit
}
extension UIKit.UIImageView {
  @_Concurrency.MainActor(unsafe) public func loadGif(name: Swift.String)
}
extension UIKit.UIImage {
  public class func gif(data: Foundation.Data) -> UIKit.UIImage?
  public class func gif(url: Swift.String) -> UIKit.UIImage?
  public class func gif(name: Swift.String) -> UIKit.UIImage?
}
extension BulbshareSDK.SDKError : Foundation.LocalizedError {
  public var errorDescription: Swift.String? {
    get
  }
  public var failureReason: Swift.String? {
    get
  }
}
public enum SDKError : Swift.Error {
  public enum ApplicationError : Swift.String, Swift.Error {
    case unknownError
    case unauthorized
    case deviceIsJailBreak
    public init?(rawValue: Swift.String)
    public typealias RawValue = Swift.String
    public var rawValue: Swift.String {
      get
    }
  }
  public enum NetworkError : Swift.String, Swift.Error {
    case noInternetConnection
    case connectionTimeOut
    case missingUrl
    case unknownError
    case internalServerError
    public init?(rawValue: Swift.String)
    public typealias RawValue = Swift.String
    public var rawValue: Swift.String {
      get
    }
  }
  public enum ServerError : Swift.String, Swift.Error {
    case unAuthorized
    case unknownError
    case userDoesNotExist
    case invalidPassword
    case invalidArguments
    case invalidBrief
    case briefNotExist
    case briefIsRemoved
    case invalidBriefType
    case invalidComment
    case invalidBriefCommentReference
    case briefCommentNotExist
    case briefCommentRemoved
    case invalidPermissions
    case invalidItemCount
    case invalidPage
    case invalidReturnStatusValue
    case brandNotExist
    case brandRemoved
    case pollNotExist
    case invalidMediaFile
    case invalidMediaType
    case invalidPollitemID
    case failedToUploadUrl
    case invalidPollitem
    case accessDenied
    case blockedAppVersion
    case deviceRemoved
    case deviceMismatch
    case NoAuthenticationHeader
    case invalidUserReference
    case invalidToken
    case wrongUserReference
    case inactiveCredentials
    case wrongAuthSecret
    case userInactive
    case userRemoved
    case requestIdExpired
    case invalidRequest
    case wrongDeviceReference
    case invalidDeviceReference
    case responseLimitReached
    case invalidResponseValue
    case noPollAvailable
    case invalidChecksumInput
    case invalidKey
    case WrongAuthenticationSecret
    public init?(rawValue: Swift.String)
    public typealias RawValue = Swift.String
    public var rawValue: Swift.String {
      get
    }
  }
  public enum JSONError : Swift.String, Swift.Error {
    case jsonDecodeError
    public init?(rawValue: Swift.String)
    public typealias RawValue = Swift.String
    public var rawValue: Swift.String {
      get
    }
  }
  case applicationError(error: BulbshareSDK.SDKError.ApplicationError)
  case networkError(error: BulbshareSDK.SDKError.NetworkError)
  case serverError(reason: BulbshareSDK.SDKError.ServerError)
  case jsonError(reason: BulbshareSDK.SDKError.JSONError)
}
public struct BSLinkInput : Swift.Encodable {
  public init(type: Swift.Int, authentication: BulbshareSDK.LinkInputModel, data: BulbshareSDK.LinkInputModel)
  public func encode(to encoder: Swift.Encoder) throws
}
@_inheritsConvenienceInitializers @objc(CDPollResponse) public class CDPollResponse : CoreData.NSManagedObject {
  @objc override dynamic public init(entity: CoreData.NSEntityDescription, insertInto context: CoreData.NSManagedObjectContext?)
  @objc deinit
}
public protocol VideoPickerDelegate : AnyObject {
  func didSelect(url: Foundation.URL?)
}
@objc open class VideoPicker : ObjectiveC.NSObject {
  public init(presentationController: UIKit.UIViewController, delegate: BulbshareSDK.VideoPickerDelegate)
  public func present(from sourceView: UIKit.UIView, sourceType: UIKit.UIImagePickerController.SourceType)
  @objc deinit
}
extension BulbshareSDK.VideoPicker : UIKit.UIImagePickerControllerDelegate {
  @_Concurrency.MainActor(unsafe) @objc dynamic public func imagePickerController(_ picker: UIKit.UIImagePickerController, didFinishPickingMediaWithInfo info: [UIKit.UIImagePickerController.InfoKey : Any])
}
extension BulbshareSDK.VideoPicker : UIKit.UINavigationControllerDelegate {
}
@objc public class BriefResponseData : ObjectiveC.NSObject, Swift.Codable {
  @objc deinit
  public func encode(to encoder: Swift.Encoder) throws
  required public init(from decoder: Swift.Decoder) throws
}
public struct BSBulbshareFeedInput : Swift.Encodable {
  public init(privateBrandID: Swift.Int)
  public func encode(to encoder: Swift.Encoder) throws
}
public struct BSSingleBulbshareInput : Swift.Encodable {
  public init(bulbshareRef: Swift.String)
  public func encode(to encoder: Swift.Encoder) throws
}
@_inheritsConvenienceInitializers @objc(CDPollCollection) public class CDPollCollection : CoreData.NSManagedObject {
  @objc override dynamic public init(entity: CoreData.NSEntityDescription, insertInto context: CoreData.NSManagedObjectContext?)
  @objc deinit
}
extension BulbshareSDK.CDPollResponse {
  @nonobjc public class func fetchRequest() -> CoreData.NSFetchRequest<BulbshareSDK.CDPollResponse>
  @objc @NSManaged dynamic public var pollitemid: Swift.Int32 {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var pollType: Swift.Int32 {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var collection: Foundation.NSSet? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var pollRequest: BulbshareSDK.CDPollBrief? {
    @objc get
    @objc set
  }
}
extension BulbshareSDK.CDPollResponse {
  @objc(addCollectionObject:) @NSManaged dynamic public func addToCollection(_ value: BulbshareSDK.CDPollCollection)
  @objc(removeCollectionObject:) @NSManaged dynamic public func removeFromCollection(_ value: BulbshareSDK.CDPollCollection)
  @objc(addCollection:) @NSManaged dynamic public func addToCollection(_ values: Foundation.NSSet)
  @objc(removeCollection:) @NSManaged dynamic public func removeFromCollection(_ values: Foundation.NSSet)
}
public struct BSAuthenticationInput : Swift.Encodable {
  public init(email: Swift.String, identifier: Swift.String, firstName: Swift.String, lastName: Swift.String, gender: Swift.String)
  public func encode(to encoder: Swift.Encoder) throws
}
public protocol SDKSettingsProtocol {
  static func sdkVersion() -> Swift.String
}
@_hasMissingDesignatedInitializers public class SDKSettings : BulbshareSDK.SDKSettingsProtocol {
  public static func sdkVersion() -> Swift.String
  @objc deinit
}
extension BulbshareSDK.SDKError.ApplicationError : Swift.Equatable {}
extension BulbshareSDK.SDKError.ApplicationError : Swift.Hashable {}
extension BulbshareSDK.SDKError.ApplicationError : Swift.RawRepresentable {}
extension BulbshareSDK.SDKError.ServerError : Swift.Equatable {}
extension BulbshareSDK.SDKError.ServerError : Swift.Hashable {}
extension BulbshareSDK.SDKError.ServerError : Swift.RawRepresentable {}
extension BulbshareSDK.SDKError.JSONError : Swift.Equatable {}
extension BulbshareSDK.SDKError.JSONError : Swift.Hashable {}
extension BulbshareSDK.SDKError.JSONError : Swift.RawRepresentable {}
extension BulbshareSDK.SDKError.NetworkError : Swift.Equatable {}
extension BulbshareSDK.SDKError.NetworkError : Swift.Hashable {}
extension BulbshareSDK.SDKError.NetworkError : Swift.RawRepresentable {}
