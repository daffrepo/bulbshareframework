Pod::Spec.new do |spec|
  spec.name         = 'BulbshareSDK'
  spec.version      = '0.1.8'
  spec.license      = "MIT"
  spec.homepage     = 'https://bitbucket.org/daffodilsoftware/bulbshare-ios-sdk'
  spec.authors      = { 'Swati Agarwal' => 'swati.agarwal@daffodilsw.com' }
  spec.summary      = 'PinePG SDK Cocoa Pod'
  spec.description  = 'PinePG SDK Cocoa Pod'
  spec.source       = { :git => 'https://swatidaffodilsw@bitbucket.org/daffodilsoftware/bulbshare-ios-sdk.git', :tag => spec.version }
# uncomment it for pod spec lint
  spec.vendored_frameworks = "BulbshareSDK/BulbshareSDK.xcframework"
# uncomment it for pod lib lint
  #spec.vendored_frameworks = "BulbshareSDK.xcframework"
  spec.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.platform = :ios
  spec.swift_version = "5.0"
  spec.ios.deployment_target  = '12.0'
spec.dependency "SDWebImage", "~> 5.12.3"
spec.dependency "SwiftLint"
spec.dependency "IQKeyboardManagerSwift"
spec.dependency "KeychainAccess"
spec.dependency "MBProgressHUD"
spec.dependency "Koloda"
spec.dependency "XCDYouTubeKit", "~> 2.15"
spec.dependency "SVProgressHUD"
spec.dependency "JDStatusBarNotification"
spec.dependency "KSToastView", "0.5.7"
spec.dependency "SCRecorder"
spec.dependency "TwitterKit"
spec.dependency "FacebookCore"
spec.dependency "FacebookLogin"
spec.dependency "FacebookShare"
spec.dependency "Alamofire"
spec.dependency "GoogleSignIn", "~> 5.0"
spec.dependency "Firebase/Core"
spec.dependency "Firebase/Auth"
end