//
//  APMediaPreviewView.h
//  Bulbshare
//
//  Created by Smiljan Kerencic on 26/09/2017.
//  Copyright © 2017 Aventa Plus d.o.o. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PBJVideoPlayerController.h"

@class APMediaPreviewView;
@protocol APMediaPreviewViewDelegate <NSObject>
    - (void)showImageEditing;
@end

@interface APMediaPreviewView : UIView<PBJVideoPlayerControllerDelegate>

@property (nonatomic) int showSelectedMedia;
@property (nonatomic) BOOL showImageEditing;
@property (weak, nonatomic) IBOutlet UIView *mainImageView;
@property (weak, nonatomic) IBOutlet UIButton *imageEditingButton;
@property (weak, nonatomic) IBOutlet UIView *mainVideoView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic) PBJVideoPlayerController *videoPlayerController;

@property (weak) id<APMediaPreviewViewDelegate> delegate;

- (void)initMediaWithData:(NSMutableDictionary *)data forMediaId:(int)mediaId;
- (void)removeTap:(UITapGestureRecognizer *)recognizer;
- (void)removeView;
@end
