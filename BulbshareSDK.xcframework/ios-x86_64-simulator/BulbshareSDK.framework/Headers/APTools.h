#import <UIKit/UIKit.h>
 
@class APTools;

@interface APTools : NSObject

+ (APTools*)sharedInstance;

- (UIColor*)hexColorForName:(NSString *)name;
- (NSString*)imageForName:(NSString *)name;
- (NSString*)stringForName:(NSString *)name;



-(void)showVideoLengthLimitErrorWithTitle:(NSString*)title message:(NSString*)message controller:(UIViewController*)controller;
@end
