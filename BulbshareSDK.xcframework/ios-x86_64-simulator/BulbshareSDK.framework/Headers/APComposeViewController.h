//
//  APComposeViewController.h
//  Bulbshare
//
//  Created by Smiljan Kerencic on 21/07/2017.
//  Copyright © 2017 Aventa Plus d.o.o. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "APImageTrayView.h"
#import "Brief.h"
#import "APRecorderViewController.h"
#import "APMediaPreviewViewController.h"
#import "TOCropViewController.h"
#import "APMediaPreviewView.h"

@class APComposeViewController;
@protocol APComposeViewControllerDelegate <NSObject>
- (void)refreshBulbshareFeed:(BaseBulbshare*)singleBulbshare; //after submition
- (void)didTakePhoto:(NSMutableDictionary*)dic;
- (void)didTakeVideo:(NSMutableDictionary*)dic;
@end
@protocol SubmitBulbshareDelegate <NSObject>
- (void)closeCreateBulbshare;
- (void)submitBulbshare:(BaseBulbshare*)singleBulbshare brief:(Brief*)brief; //after submition
@end


@interface APComposeViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, APImageTrayViewDelegate, UITextViewDelegate, UITextFieldDelegate, APRecorderViewControllerDelegate, APMediaLibraryViewControllerDelegate, APMediaPreviewViewControllerDelegate, TOCropViewControllerDelegate, APMediaPreviewViewDelegate>

@property (strong, nonatomic) NSString *selectedBriefRef;
@property (strong, nonatomic) Brief *passingObject;
@property (nonatomic) NSString *fromSegue;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *colorChangeButton;
@property (weak, nonatomic) IBOutlet UIButton *addTitleButton;
@property (weak, nonatomic) IBOutlet APImageTrayView *imageTrayView;
@property (weak, nonatomic) IBOutlet UIView *toolsView;
@property (weak, nonatomic) IBOutlet UIScrollView *imageTrayScrollView;
@property (weak, nonatomic) IBOutlet UIView *imageTrayMainView;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak) id<APComposeViewControllerDelegate> delegate;
@property (weak) id<SubmitBulbshareDelegate> submitBulbshareDelegate;


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *imageTrayConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *latoutBottomConstraint;

@end
