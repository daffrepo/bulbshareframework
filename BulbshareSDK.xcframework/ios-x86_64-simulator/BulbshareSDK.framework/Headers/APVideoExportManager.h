#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <Photos/Photos.h>
#import "SDAVAssetExportSession.h"

@class APVideoExportManager;

@protocol APVideoExportManagerDelegate <NSObject>
    - (void)videoExportFailedWithError:(NSError*)error mode:(int)mode item:(int)item;
    - (void)videoExportCompletedWithVideo:(NSString*)url mode:(int)mode item:(int)item;
    - (void)videoExportExportingWithProgress:(float)progress mode:(int)mode item:(int)item;
    - (void)videoExportCancelledForItem:(int)item;
@end

@interface APVideoExportManager : NSObject<SDAVAssetExportSessionDelegate>

+ (APVideoExportManager*)sharedInstance;

- (void)exportVideoMedia:(AVURLAsset*)asset mode:(int)mode item:(int)item;
- (NSString*)saveVideoMedia:(NSURL*)asset;
- (NSString*)saveVideoMedia:(NSURL*)videoUrl withName:(NSString*)name;
- (NSString*)removeVideoMedia:(NSURL*)asset;
- (NSString*)saveMediaFile:(UIImage*)img forType:(NSString*)type;
- (NSString*)saveMediaUrl:(NSURL*)videoUrl forType:(NSString*)type;
@property(weak) id<APVideoExportManagerDelegate> delegate;

@end
