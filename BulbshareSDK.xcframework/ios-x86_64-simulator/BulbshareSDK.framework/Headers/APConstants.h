#ifdef __OBJC__
    #import <UIKit/UIKit.h>
#endif

#define CASE(str) if ([__s__ isEqualToString:(str)])
#define SWITCH(s) for (NSString *__s__ = (s); ; )
#define DEFAULT

#define ACCEPTABLE_CHARACTERS @"!~`@#$%^&*-+();:=_{}[],.<>?\\/|\"\'0123456789"
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kSemiModalDidShowNotification @"kSemiModalDidShowNotification"
#define kSemiModalDidHideNotification @"kSemiModalDidHideNotification"
#define kSemiModalWasResizedNotification @"kSemiModalWasResizedNotification"
#define kInstagramSegueToBePerformed @"kInstagramSegueToBePerformed"

#define kTwitterUploadUrl @"https://upload.twitter.com/1.1/media/upload.json"
#define kTwitterStatusesUrl @"https://api.twitter.com/1.1/statuses/update.json"

extern NSString * const BS_AUTH_SECRET;
extern NSString * const BS_PASS_UPDATE;
extern NSString * const BS_INSERT_PASSCODE;
extern NSString * const BS_DEV_REF;
extern NSString * const BS_USER_REF;
extern NSString * const BS_PASS_PREFIX;
extern NSString * const BS_PASS_PREFIX_1;
extern NSUInteger const BS_MIN_PASS_LENGTH;
extern NSUInteger const BS_MAX_VIDEO_LENGTH;
extern NSUInteger const BS_MAX_LONG_VIDEO_LENGTH;
extern NSUInteger const BS_MAX_SURVEY_VIDEO_LENGTH;
extern NSUInteger const BS_MAX_SURVEY_AUDIO_LENGTH;
extern NSUInteger const BS_MIN_SURVEY_VIDEO_LENGTH;
extern NSUInteger const BS_MIN_SURVEY_AUDIO_LENGTH;
extern NSUInteger const BS_MAX_COUNTER_OBJECTS;
extern NSString * const BS_MAX_LIST_OBJECTS;
extern NSString * const BS_SWIPE_REMOVE_DATE;
extern NSString * const BS_BRIEF_ITEMS_REMOVE_DATE;
extern NSUInteger const BS_MAX_MARKUP_LIMIT;
extern NSUInteger const BS_MAX_MARKUP_COMMENT_LIMIT;
extern NSUInteger const BS_UPLOAD_RETRY_LIMIT;
extern NSUInteger const BS_OTHER_COMMENT_LIMIT;
extern NSUInteger const BS_INSERT_COMMENT_LIMIT;
extern NSString * const BS_BULBSHARE_TEAM;

//extern NSString * const BS_USERS_THEME;
extern NSString * const BS_NO_DEF_BRAND;
extern NSUInteger const BS_IMAGE_SCALE;
extern NSString * const BS_FEATURE_FILTER;

extern NSString * const BS_APPSFLYER_DEV_KEY;

extern NSString * const BS_DEF_CLIENT_ID;
extern NSString * const BS_DEF_SECRET;

extern NSString * const BS_BSHR_URL;
extern NSString * const BS_REACHABILITY_URL;
extern NSString * const BS_URL_HTTPS;
extern NSString * const BS_BASE_DEV_URL_INSTANCE;
extern NSString * const BS_BASE_DEV_URL;
extern NSString * const BS_BASE_URL;
extern NSString * const BS_BASE_MAIN_URL;
extern NSString * const BS_BASE_URL_INSTANCE;
extern NSString * const BS_BULBSHARE_LINK_PARAMETER;
extern NSString * const BS_LAYER;
extern NSString * const BS_LAYER_INSTANCES;
extern NSString * const BS_FAQ;
extern NSString * const BS_EULA;
extern NSString * const BS_PRIVACY_NOTICE;
extern NSString * const BS_ITUNES_URL;
extern NSString * const BS_APP_INVITE_URL;
extern NSString * const BS_APP_INVITE_IMAGE;
extern NSString * const BS_INSTAGRAM_HOWTO;
extern NSString * const BS_WHATSAPP_HOWTO;

extern NSString * const BS_PING_METHOD;
extern NSString * const BS_PING_AUTH_METHOD;

//Instagram
extern NSString * const INSTAGRAM_AUTHURL;
extern NSString * const INSTAGRAM_APIURl;
extern NSString * const INSTAGRAM_CLIENT_ID;
extern NSString * const INSTAGRAM_CLIENTSERCRET;
extern NSString * const INSTAGRAM_REDIRECT_URI;
extern NSString * const INSTAGRAM_ACCESS_TOKEN;
extern NSString * const INSTAGRAM_SCOPE;

//Method calls
extern NSString * const METHOD_FB_CONNECT;
extern NSString * const METHOD_LOGOUT;
extern NSString * const METHOD_PROFILE;
extern NSString * const METHOD_GET_PROFILE;
extern NSString * const METHOD_GET_BRAND;
extern NSString * const METHOD_MY_FEED;
extern NSString * const METHOD_MY_FEED_PE;
extern NSString * const METHOD_MY_FEED_CP;
extern NSString * const METHOD_MY_BULBSHARE_FEED;
extern NSString * const METHOD_GET_BRIEF;
extern NSString * const METHOD_SUBMIT_BULBSHARE;
extern NSString * const METHOD_SUBMIT_VIDEO;
extern NSString * const METHOD_SUBMIT_PICTURE;
extern NSString * const METHOD_GET_BULBSHARE_BY_BRIEF;
extern NSString * const METHOD_GET_BULBSHARE_BY_USER;
extern NSString * const METHOD_GET_BULBSHARE_BY_REF;
extern NSString * const METHOD_GET_BRIEFS_BY_BRAND;
extern NSString * const METHOD_GET_REWARDS;
extern NSString * const METHOD_GET_REWARD;
extern NSString * const METHOD_GET_BULBSHARE_COMMENTS;
extern NSString * const METHOD_GET_BRIEF_COMMENTS;
extern NSString * const METHOD_SUBMIT_BULBSHARE_COMMENTS;
extern NSString * const METHOD_SUBMIT_BRIEF_COMMENTS;
extern NSString * const METHOD_SEARCH_BRIEF;
extern NSString * const METHOD_SEARCH_BRIEF;
extern NSString * const METHOD_SEARCH_USERS;
extern NSString * const METHOD_DISCOVER_USERS;
extern NSString * const METHOD_SEARCH_BRANDS;
extern NSString * const METHOD_DISCOVER_BRANDS;
extern NSString * const METHOD_GET_BRAND_FOLLOWERS;
extern NSString * const METHOD_GET_BULBSHARE_LIKES;
extern NSString * const METHOD_GET_BRIEF_LIKES;
extern NSString * const METHOD_GET_BULBSHARE_LIKES_BY_BRIEF;
extern NSString * const METHOD_SET_FOLLOW_BRAND;
extern NSString * const METHOD_SET_FOLLOW_USER;
extern NSString * const METHOD_SET_UNFOLLOW_BRAND;
extern NSString * const METHOD_SET_UNFOLLOW_USER;
extern NSString * const METHOD_GET_FOLLOWED_BRANDS;
extern NSString * const METHOD_GET_FOLLOWED_USERS;
extern NSString * const METHOD_SET_LIKE_BULBSHARE;
extern NSString * const METHOD_SET_LIKE_BRIEF;
extern NSString * const METHOD_SET_LIKE_BRAND;
extern NSString * const METHOD_SET_LIKE_USER;
extern NSString * const METHOD_SET_UNLIKE_BULBSHARE;
extern NSString * const METHOD_SET_UNLIKE_BRIEF;
extern NSString * const METHOD_SET_UNLIKE_BRAND;
extern NSString * const METHOD_SET_UNLIKE_USER;
extern NSString * const METHOD_REPORT_USER;
extern NSString * const METHOD_BLOCK_USER;
extern NSString * const METHOD_UNBLOCK_USER;
extern NSString * const METHOD_SET_FOLLOW_USER_BYFBID;
extern NSString * const METHOD_SET_UNFOLLOW_USER_BYFBID;
extern NSString * const METHOD_REGISTER_DEVICE_FOR_NOTIFICATIONS;
extern NSString * const METHOD_UNREGISTER_DEVICE_FOR_NOTIFICATIONS;
extern NSString * const METHOD_LOGIN;
extern NSString * const METHOD_UPDATE_PASSWORD;
extern NSString * const METHOD_FORGOTTEN_PASSWORD;
extern NSString * const METHOD_GET_BRANDS;
extern NSString * const METHOD_GET_BRAND_PRIVATE_ACCESS_BY_BRIEF;
extern NSString * const METHOD_GET_BRAND_PRIVATE_ACCESS_BY_BRAND;
extern NSString * const METHOD_GET_LIST_BRANDS_PRIVATE_ACCESS;
extern NSString * const METHOD_DELETE_BULBSHARE;
extern NSString * const METHOD_DELETE_BULBSHARE_COMMENT;
extern NSString * const METHOD_DELETE_BRIEF_COMMENT;
extern NSString * const METHOD_GET_BRAND_PRIVATE_ACCESS_BY_PASSCODE;
extern NSString * const METHOD_REVOKE_PRIVATE_ACCESS;
extern NSString * const METHOD_REVOKE_PRIVATE_ACCESS_BY_BRAND;
extern NSString * const METHOD_GET_CUSTOM_THEME;
extern NSString * const METHOD_SET_AVATAR;
extern NSString * const METHOD_MY_FEED_BULBSHARES;
extern NSString * const METHOD_GET_USER_FOLLOWERS;
extern NSString * const METHOD_GET_NOTIFICATIONS;
extern NSString * const METHOD_GET_CITIES_FOR_COUNTRY_BY_CODE;
extern NSString * const METHOD_GET_CITIES_FOR_COUNTRY_BY_ID;
extern NSString * const METHOD_GET_COUNTRIES;
extern NSString * const METHOD_CHECK_EMAIL;
extern NSString * const METHOD_EMAIL_CONNECT;
extern NSString * const METHOD_UPDATE_PROFILE;
extern NSString * const METHOD_GET_BULBSHARES_AND_THEME_BY_BRIEF;
extern NSString * const METHOD_MARK_NOTIFICATIONS_ASREAD;
extern NSString * const METHOD_MARK_NOTIFICATION_ASREAD;
extern NSString * const METHOD_UPDATE_INTERESTS;
extern NSString * const METHOD_GET_SOCIAL_ACCOUNTS;
extern NSString * const METHOD_ADD_SOCIAL_ACCOUNT;
extern NSString * const METHOD_REMOVE_SOCIAL_ACCOUNT;
extern NSString * const METHOD_GET_INTERESTS;
extern NSString * const METHOD_SET_FACEBOOK_FRIENDS;
extern NSString * const METHOD_SET_DEFAULT_ENVIRONMENT;
extern NSString * const METHOD_GET_DEFAULT_ENVIRONMENT;
extern NSString * const METHOD_UPLOAD_FILE;
extern NSString * const METHOD_SET_INSTANCE;
extern NSString * const METHOD_GET_BULBSHARE_SHARE_URL;
extern NSString * const METHOD_GET_BRIEF_POLL;
extern NSString * const METHOD_SET_BRIEF_POLL_RESPONSE;
extern NSString * const METHOD_SET_BRAND_ACCESS_NO_SCREENER;
extern NSString * const METHOD_GET_BRAND_BY_REF;
extern NSString * const METHOD_GET_S3_PRESIGNED_URL;
extern NSString * const METHOD_SET_S3_PRESIGNED_URL;
extern NSString * const METHOD_GET_S3_MEDIA_PRESIGNED_URL;
extern NSString * const METHOD_SET_S3_MEDIA_PRESIGNED_URL;
extern NSString * const METHOD_MAGIC_LINK;
extern NSString * const METHOD_APPLE_CONNECT;
extern NSString * const METHOD_CHECK_MAINTENANCE;
extern NSString * const METHOD_RECORD_SOCIAL_ACCOUNT_PRESSED;
extern NSString * const METHOD_GET_S3_BULBSHARE_MEDIA_URL;
extern NSString * const METHOD_SET_S3_BULBSHARE_MEDIA_PRESIGNED_URL;
extern NSString * const METHOD_CHECK_INVITE_CODE;
extern NSString * const METHOD_GET_START_POLL_RESPONSE;

// Cell constants
extern NSString * const MY_FEED_CELL;
extern NSString * const MY_PROMPT_CELL;
extern NSString * const MY_FEED_UPLOAD_CELL;
extern NSString * const MY_FEED_COMMENTS_CELL;
extern NSString * const MY_REWARDS_CELL;
extern NSString * const SEARCH_TOOLBAR_CELL;
extern NSString * const FRIENDS_CELL ;
extern NSString * const MY_COMMENT_CELL;
extern NSString * const POST_COMMENT_LIKE_CELL;
extern NSString * const COMPOSE_TITLE_CELL;
extern NSString * const COMPOSE_COMMENT_CELL;
extern NSString * const COMPOSE_COMMENT_CHAR_COUNT_CELL;
extern NSString * const COMPOSE_MEDIA_CELL;
extern NSString * const CONTENT_REPORT_CELL;
extern NSString * const CONTENT_REPORT_HEADER;


// Brief cell constants
extern NSString * const BRIEF_HEADER_CELL;
extern NSString * const BRIEF_BASIC_INFO_CELL;
extern NSString * const BRIEF_SHORT_DESCRIPTION_CELL;
extern NSString * const BRIEF_LONG_DESCRIPTION_CELL;
extern NSString * const BRIEF_AD1IMAGE_CELL;
extern NSString * const BRIEF_AD2IMAGE_CELL;
extern NSString * const BRIEF_CREATE_CELL;
extern NSString * const BRIEF_BULBSHARES_CELL;
extern NSString * const BRIEF_SUMMARY_CELL;
extern NSString * const BRIEF_JOIN_BRIEF_CELL;
extern NSString * const BRIEF_BRIEF_CELL;
extern NSString * const BRIEF_BRIEF_TITLE_CELL;
extern NSString * const BRIEF_BLANK_CELL;

//Join Channel Cell Constants
extern NSString * const CHANNEL_JOIN_BASIC_CELL;
extern NSString * const CHANNEL_JOIN_LOGO_CELL;
extern NSString * const CHANNEL_JOIN_TITLE_CELL;
extern NSString * const CHANNEL_JOIN_DESCRIPTION_CELL;
extern NSString * const CHANNEL_JOIN_JOIN_CELL;
extern NSString * const CHANNEL_JOIN_CONFIRMATION_DESCRIPTION_CELL;
extern NSString * const CHANNEL_JOIN_COLOR_LINE_SPACER_CELL;
extern NSString * const CHANNEL_JOIN_GRAY_LINE_SPACER_CELL;
extern NSString * const CHANNEL_JOIN_HTML_CELL;
extern NSString * const CHANNEL_JOIN_JOIN_VIA_PASSCODE_CELL;

// Segue constants (prefix SEGUE_)
extern NSString * const SEGUE_BRAND_PROFILE;
extern NSString * const SEGUE_PERSON_PROFILE;
extern NSString * const SEGUE_APP_PERSON_PROFILE;
extern NSString * const SEGUE_LONG_DESCRIPTION;
extern NSString * const SEGUE_USER_PROFILE;
extern NSString * const SEGUE_LOGIN_SUCCESS;
extern NSString * const SEGUE_MY_BRIEF;
extern NSString * const SEGUE_MY_BRIEF_SINGLE;
extern NSString * const SEGUE_BRIEF_RESPONSE;
extern NSString * const SEGUE_BRIEF_LONG_VIDEO_RESPONSE;
extern NSString * const SEGUE_BULBSHARE_FEED;
extern NSString * const SEGUE_BULBSHARE_FEED_NO_ANIMATION;
extern NSString * const SEGUE_BULBSHARE_COMMENTS;
extern NSString * const SEGUE_BRIEF_COMMENTS;
extern NSString * const SEGUE_SINGLE_BULBSHARE;
extern NSString * const SEGUE_LIST;
extern NSString * const SEGUE_MY_REWARDS;
extern NSString * const SEGUE_MY_REWARD;
extern NSString * const SEGUE_SETTINGS;
extern NSString * const SEGUE_CUSTOM_LOGIN;
extern NSString * const SEGUE_UPDATE_PASSWORD;
extern NSString * const SEGUE_FORGOTTEN_PASSWORD;
extern NSString * const SEGUE_INSERT_PASSCODE;
extern NSString * const SEGUE_PREVIEV;
extern NSString * const SEGUE_FACEBOOK_FRIENDS;
extern NSString * const SEGUE_CHANGE_BRAND;
extern NSString * const SEGUE_LOADING;
extern NSString * const SEGUE_NOTIFICATIONS;
extern NSString * const SEGUE_REGISTER_STEP_ONE;
extern NSString * const SEGUE_REGISTER_STEP_TWO;
extern NSString * const SEGUE_REGISTER_STEP_THREE;
extern NSString * const SEGUE_REGISTER_STEP_FOUR;
extern NSString * const SEGUE_REGISTER_STEP_FIVE;
extern NSString * const SEGUE_REGISTER_STEP_SIX;
extern NSString * const SEGUE_CHANGE_USER_PASSWORD;
extern NSString * const SEGUE_EDIT_USER_INFO;
extern NSString * const SEGUE_MANAGE_BRANDS;
extern NSString * const SEGUE_SOCIAL_ACCOUNTS;
extern NSString * const SEGUE_INVITE_FRIENDS;
extern NSString * const SEGUE_COUNTRY_LIST;
extern NSString * const SEGUE_CITY_LIST;
extern NSString * const SEGUE_TUTORIAL;
extern NSString * const SEGUE_INTERESTS;
extern NSString * const SEGUE_TAG_INTERESTS_BRANDS;
extern NSString * const SEGUE_UNLINK_SOCIAL_ACCOUNTS;
extern NSString * const SEGUE_ABOUT;
extern NSString * const SEGUE_MEDIA_PREVIEW;
extern NSString * const SEGUE_RECORDER;
extern NSString * const SEGUE_MEDIA_LIBRARY;
extern NSString * const SEGUE_INSTANCE_CUSTOM_LOGIN;
extern NSString * const SEGUE_SOCIAL_COMPOSER;
extern NSString * const SEGUE_POLL_BRIEF_RESPONSE;
extern NSString * const SEGUE_CONTENT_REPORT;
extern NSString * const SEGUE_JOIN_CHANNEL;
extern NSString * const SEGUE_JOIN_CHANNEL_FROM_FEED;
extern NSString * const SEGUE_AIRSHIP_SINGLE_MEESAGE;
extern NSString * const SEGUE_ADD_INSTRAGRAM_FROM_GENERIC;

// Saved object keys for simple caching (prefix STORAGE_KEY_ )
extern NSString * const STORAGE_KEY_BASE_URL;
extern NSString * const STORAGE_KEY_APP_PERSON;
extern NSString * const STORAGE_KEY_APP_PERSON_REFERRER_CODE;
extern NSString * const STORAGE_KEY_DEVICE;
extern NSString * const STORAGE_KEY_MY_FEEDS;
extern NSString * const STORAGE_KEY_BRIEF;
extern NSString * const STORAGE_KEY_BRIFS_BULBSHARES;
extern NSString * const STORAGE_KEY_BRAND_BULBSHARES;
extern NSString * const STORAGE_KEY_MY_BRANDS;
extern NSString * const STORAGE_KEY_CACHE_BRIEF_VIDEOS;
extern NSString * const STORAGE_KEY_CACHE_BULBSHARE_VIDEOS;
extern NSString * const STORAGE_KEY_BRANDS_BRIEFS;
extern NSString * const STORAGE_KEY_MY_NOTIFICATIONS;
extern NSString * const STORAGE_KEY_DID_UPDATE_APP_PERSON;
extern NSString * const STORAGE_KEY_BULBSHARE;
extern NSString * const STORAGE_KEY_UPLOADING_BULBSHARES;
extern NSString * const STORAGE_KEY_UPLOADING_BULBSHARE_LONG_VIDEOS;
extern NSString * const STORAGE_KEY_UPLOADING_BULBSHARES_PROGRESS;
extern NSString * const STORAGE_KEY_SAVE_BULBSHARES;
extern NSString * const STORAGE_KEY_SHOULD_SAVE_IMAGE;
extern NSString * const STORAGE_KEY_SHOULD_SAVE_VIDEO;
extern NSString * const STORAGE_KEY_USERS_SOCIAL_ACCOUNTS;
extern NSString * const STORAGE_KEY_POLL_BRIEF_RESPONSE;
extern NSString * const STORAGE_KEY_SELECTED_BRIEF;
extern NSString * const STORAGE_KEY_SURVEY_MEDIA;
extern NSString * const STORAGE_KEY_APP_STATE_FOREGROUND;
extern NSString * const STORAGE_KEY_APPLE_SIGNIN_EMAIL;
extern NSString * const STORAGE_KEY_APPLE_SIGNIN_NAME;


// theme color keys
extern NSString * const COLOR_KEY_MAIN_THEME_LIGHT;
extern NSString * const COLOR_KEY_MAIN_THEME_DARK;
extern NSString * const COLOR_KEY_JOIN_BUTTON_LIGHT_BACKGROUND;
extern NSString * const COLOR_KEY_JOIN_BUTTON_TEXT;
extern NSString * const COLOR_KEY_FOLLOW_BUTTON_BACKGROUND;
extern NSString * const COLOR_KEY_FOLLOW_BUTTON_TEXT;
extern NSString * const COLOR_KEY_EMAIL_LOGIN_BUTTON_BACKGROUND;
extern NSString * const COLOR_KEY_EMAIL_LOGIN_BUTTON_TEXT;

//Storyboard ID's (prefix STORYBOARD_ID)
extern NSString * const STORYBOARD_ID_MAIN_TAB_BAR_CONTROLLER;
extern NSString * const STORYBOARD_ID_LOGIN_VIEW_CONTROLLER;
extern NSString * const STORYBOARD_ID_INSERT_PASSCODE_VIEW_CONTROLLER;
extern NSString * const STORYBOARD_ID_REGISTER_INSERT_PASSCODE_VIEW_CONTROLLER;
extern NSString * const STORYBOARD_ID_JOIN_CHANNEL_VIEW_CONTROLLER;

//Google Analytics
extern NSString * const BS_GA_APIKEY;
extern NSString * const BS_kAllowTracking;
extern NSString * const BS_kGoogleClientId;

//Google Analytics screen tracking ID
extern NSString * const BS_SEARCH_SCREEN_TRACKING;
extern NSString * const BS_BRIEF_SCREEN_TRACKING;
extern NSString * const BS_BRIEF_FEED_SCREEN_TRACKING;
extern NSString * const BS_BULBSHARE_FEED_SCREEN_TRACKING;
extern NSString * const BS_BULBSHARE_SUBMIT_SCREEN_TRACKING;
extern NSString * const BS_POST_COMMENT_SCREEN_TRACKING;
extern NSString * const BS_INTRO_SCREEN_TRACKING;
extern NSString * const BS_SETTINGS_SCREEN_TRACKING;
extern NSString * const BS_LONG_DESCRIPTION_SCREEN_TRACKING;
extern NSString * const BS_BRIEF_LIKES_SCREEN_TRACKING;
extern NSString * const BS_BULBSHARE_LIKES_SCREEN_TRACKING;
extern NSString * const BS_LOGIN_SCREEN_TRACKING;
extern NSString * const BS_CUSTOM_LOGIN_SCREEN_TRACKING;
extern NSString * const BS_BRAND_PROFILE_SCREEN_TRACKING;
extern NSString * const BS_PERSON_PROFILE_SCREEN_TRACKING;
extern NSString * const BS_MY_PROFILE_SCREEN_TRACKING;
extern NSString * const BS_MY_REWARDS_SCREEN_TRACKING;
extern NSString * const BS_MY_REWARD_DESCRIPTION_SCREEN_TRACKING;
extern NSString * const BS_UPDATE_PASSWORD_SCREEN_TRACKING;
extern NSString * const BS_FORGOTT_PASSWORD_SCREEN_TRACKING;
extern NSString * const BS_INSERT_PASSCODE_SCREEN_TRACKING;
extern NSString * const BS_USER_FOLLOWERS_SCREEN_TRACKING;
extern NSString * const BS_USER_FOLLOWING_SCREEN_TRACKING;
extern NSString * const BS_BRAND_FOLLOWERS_SCREEN_TRACKING;
extern NSString * const BS_SINGLE_BULBSHARE_SCREEN_TRACKING;
extern NSString * const BS_BULBSHARE_PREVIEW_SCREEN_TRACKING;
extern NSString * const BS_POST_BRIEF_COMMENT_SCREEN_TRACKING;
extern NSString * const BS_POST_BULBSHARE_COMMENT_SCREEN_TRACKING;
extern NSString * const BS_BRIEF_LIST_FEED_SCREEN_TRACKING;
extern NSString * const BS_BULBSHARE_LIST_FEED_SCREEN_TRACKING;
extern NSString * const BS_NOTIFICATION_SCREEN_TRACKING;
extern NSString * const BS_REGISTRATION_EMAILPASSWORD_SCREEN_TRACKING;
extern NSString * const BS_REGISTRATION_PHONE_SCREEN_TRACKING;
extern NSString * const BS_REGISTRATION_COUNTRYCITY_SCREEN_TRACKING;
extern NSString * const BS_REGISTRATION_BIRTHDATEGENDER_SCREEN_TRACKING;
extern NSString * const BS_REGISTRATION_NAMELASTNAME_SCREEN_TRACKING;
extern NSString * const BS_REGISTRATION_FINISH_SCREEN_TRACKING;
extern NSString * const BS_TUTORIAL_SCREEN_TRACKING;
extern NSString * const BS_INTEREST_SCREEN_TRACKING;
extern NSString * const BS_REGISTRATION_INTEREST_SCREEN_TRACKING;
extern NSString * const BS_CHANGE_PASSWORD_TRACKING;
extern NSString * const BS_SOCIAL_ACCOUNTS_TRACKING;
extern NSString * const BS_UNLINK_SOCIAL_ACCOUNTS_TRACKING;
extern NSString * const BS_EDIT_PROFILE_TRACKING;
extern NSString * const BS_MANAGE_BRANDS_TRACKING;
extern NSString * const BS_INSERT_INSTANCE_SCREEN_TRACKING;

//Engage TAGS
extern NSString * const ENG_USERS_FIRST_LOGIN_TAG;
extern NSString * const ENG_USERS_CHANNEL_TAG;
extern NSString * const ENG_USERS_ORGANISATION_TAG;
extern NSString * const ENG_GENERAL_BRIEF_OPENED_TAG;
extern NSString * const ENG_GENERAL_BRIEF_STARTED_TAG;
extern NSString * const ENG_GENERAL_BRIEF_RESPONDED_TAG;
extern NSString * const ENG_SURVEY_BRIEF_OPENED_TAG;
extern NSString * const ENG_SURVEY_BRIEF_STARTED_TAG;
extern NSString * const ENG_SURVEY_BRIEF_RESPONDED_TAG;
extern NSString * const ENG_USER_COUNTRY_UPDATED_TAG;
extern NSString * const ENG_USER_CITY_UPDATED_TAG;
extern NSString * const ENG_USERS_SOCIAL_ACCOUNT_TAG;
extern NSString * const ENG_USER_AVATAR_UPDATED_TAG;
extern NSString * const ENG_USER_FACEBOOK_REGISTERED_TAG;

//Engage screen tracking
extern NSString * const ENG_FEED_SCREEN_TRACKING;
extern NSString * const ENG_INBOX_SCREEN_TRACKING;
extern NSString * const ENG_LOGIN_SCREEN_TRACKING;
extern NSString * const ENG_PROFILE_SCREEN_TRACKING;
extern NSString * const ENG_PROFILE_EDIT_SCREEN_TRACKING;
extern NSString * const ENG_REGISTER_SCREEN_TRACKING;
extern NSString * const ENG_REWARDS_LIST_SCREEN_TRACKING;
extern NSString * const ENG_SEARCH_SCREEN_TRACKING;
extern NSString * const ENG_SETTINGS_SCREEN_TRACKING;
